package de.artismarti.news

import javax.persistence.GeneratedValue
import javax.persistence.Id
import javax.persistence.MappedSuperclass

/**
 * Super type of all entities.
 *
 * @author artur
 */
@MappedSuperclass
open class SuperEntity {
    @Id
    @GeneratedValue
    public var id: Long = -1
}
