package de.artismarti.news

import javax.persistence.Entity

/**
 * @author artur
 */
@Entity
class News() : SuperEntity() {

    var title: String = ""
    var text: String = ""
    var image: String = ""
    var date: Long = -1
    var author: String = ""

}
