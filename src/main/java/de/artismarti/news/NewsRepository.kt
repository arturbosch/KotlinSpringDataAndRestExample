package de.artismarti.news

import org.springframework.data.repository.PagingAndSortingRepository
import org.springframework.data.repository.query.Param
import org.springframework.data.rest.core.annotation.RepositoryRestResource

/**
 * @author artur
 */
@RepositoryRestResource(path = "news", collectionResourceRel = "news")
interface NewsRepository : PagingAndSortingRepository<News, Long> {

    fun findByAuthor(@Param("author") author: String): List<News>
}
